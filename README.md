<!-- PROJECT DESCRIPTION -->

# Beginner's guide to creating an API <a name="about-project"></a>

**Creating API** Beginner's guide to creating an API simple web application. User can get, create, update, delete, menu items. 
Built with ruby on rails.

## 🛠 Built With <a name="built-with">Ruby, Ruby on Rails, Docker, SQLite3</a>


<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://rubyonrails.org/">Ruby on Rails</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.sqlite.org/">SQLite3</a></li>
  </ul>
</details>

### Key Features <a name="key-features"></a>

- **CRUD task by using HTTP resquests**


<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

```sh
 gem install rails
```
```sh
 Docker
```

### Setup

Clone this repository to your desired folder:

```sh
  git clone git@gitlab.com:oshie_15/beginners-guide-to-creating-an-api.git

```


```sh
  cd beginners-guide-to-creating-an-api
  and switch to the correct branch name `features`
```

### Usage

To run the project, execute the following command:

```sh
  docker-compose build
```
```sh
  docker-compose up -d
```
### Install

```sh 
  rails db:migrate
``` 
```sh 
  rails db:seed
``` 

### Postman
```sh
1.Set HTTP Method to GET
2.Enter request URL of http://localhost:3000/secret_menu_items
3.Press send
```
```sh
1.Set HTTP Method to GET
2.Enter request URL of http://localhost:3000/secret_menu_items/1
3.Press send
```
```sh
1.Set HTTP Method to POST
2.Enter request URL of http://localhost:3000/secret_menu_items
3.Open Body tab write key and values based on attrubtes
```
```sh
1.Set HTTP Method to PATCH
2.Enter request URL of http://localhost:3000/secret_menu_items/4
3.Open Body tab
```

## 👥 Authors <a name="Oshie" />

👤 **Oshie**

- GitLab: [GitLab Profile](https://gitlab.com/oshie_15)

## 🤝 Contributing <a name="contributing"></a>

Contributions, issues, and feature requests are welcome!
